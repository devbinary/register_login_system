<?php
function change_profile_image($user_id, $file_temp, $file_extn) {
//    $file_name = substr(md5(time()), 0, 10) . '.' . $file_extn;
    $file_path =  'images/profile/' . substr(md5(time()), 0, 10) . '.' . $file_extn;
//    echo $file_name;
//    echo $file_path;
    move_uploaded_file($file_temp, $file_path);
    mysql_query("UPDATE `users` 
                SET `profile` = '" . mysql_real_escape_string($file_path) . "' 
                WHERE `user_id` = " . (int)$user_id);
}

function mail_users($subject, $body) {
    $query = mysql_query("SELECT `email`, `first_name` 
                          FROM `users` 
                          WHERE `allow_email` = 1");
    while (($row = mysql_fetch_assoc($query)) !== false) {
//        $body = "Hello " . $row['first_name'] . ",\n\n" . $body;
        email($row['email'], $subject, "Hello " . $row['first_name'] . ",\n\n" . $body);
    }
}

// function is_admin($user_id) {
//    $user_id = (int)$user_id;
//    $query = mysql_query("SELECT COUNT(`user_id`) 
//                          FROM `users` 
//                          WHERE `user_id` = $user_id 
//                          AND `type` = 1");
//    return (mysql_result($query, 0) == 1) ? true : false;
// }

function has_access($user_id, $type) {
    $user_id = (int)$user_id;
    $type    = (int)$type;
    $query = mysql_query("SELECT COUNT(`user_id`) 
                          FROM `users` 
                          WHERE `user_id` = $user_id 
                          AND `type` = $type");
    return (mysql_result($query, 0) == 1) ? true : false;
}

function recover($mode, $email) {
    $mode   = sanitize($mode);
    $email  =  sanitize($email);
    
    $user_data = user_data(user_id_from_email($email), 'user_id', 'first_name', 'username');
    
    if ($mode == 'username') {
        // recover username
        email($email, 'Your username', "Hello " . $user_data['first_name'] . ",\n\nYour username is: " . $user_data['username'] . "\n\n-sparklet");
    } else if ($mode == 'password'){
        // recover password
        $generated_password = substr(md5(rand(999, 999999)), 0, 8);
        // die($generated_password);
        change_password($user_data['user_id'], $generated_password);
        
        update_user($user_data['user_id'], array('password_recover' => '1'));
        
        email($email, 'Your password recovery', "Hello " . $user_data['first_name'] . ",\n\nYour new password is: " . $generated_password . "\n\n-sparklet");
    }
}

function update_user($user_id, $update_data) {
//    global $session_user_id;
    $update = array();
    array_walk($update_data, 'array_sanitize');
    
    foreach($update_data as $field=>$data) {
        $update[] = ' `' . $field . '` = \'' . $data . '\'';
    }
    
//    print_r($update);
//    echo implode(', ', $update);
//    die();
 
// option with global:    
//    mysql_query("UPDATE `users` SET " . implode(', ', $update) . " WHERE `user_id` = $session_user_id");
// option without global:
//    mysql_query("UPDATE `users` SET " . implode(', ', $update) . " WHERE `user_id` = " . $_SESSION['user_id']) or die(mysql_error());
// option with $user_id
    mysql_query("UPDATE `users` SET " . implode(', ', $update) . " WHERE `user_id` = $user_id");
}
    
function activate($email, $email_code) {
    $email      = mysql_real_escape_string($email);
    $email_code = mysql_real_escape_string($email_code);
        // query to update user active status
    $query = mysql_query("SELECT COUNT(`user_id`) 
                    FROM `users` 
                    WHERE `email` = '$email' 
                    AND `email_code` = `$email_code` 
                    AND `active` = 0");
    // if (mysql_result($query, 0) == 1) {
    // changed 1 to 0
    if (mysql_result($query, 0) == 0) {
        mysql_query("UPDATE `users` SET `active` = 1 
                     WHERE `email` = '$email'");
        return true;
    } else {
        return false;
    }
}

function change_password($user_id, $password) {
    $user_id = (int)$user_id;
    $password = md5($password);
    
    mysql_query("UPDATE `users` 
                SET `password` = '$password', 
                `password_recover` = 0   
                WHERE `user_id` = $user_id");
}

function register_user($register_data) {
    array_walk($register_data, 'array_sanitize');
    $register_data['password'] = md5($register_data['password']);
//    print_r($register_data);

    $fields = '`' . implode('`, `', array_keys($register_data)) . '`';
//    echo $data;
    $data = '\'' . implode('\', \'', $register_data) . '\'';
//    echo $fields;
//    echo "INSERT INTO `users` ($fields) VALUES ($data)";
//    die();
    mysql_query("INSERT INTO `users` ($fields) VALUES ($data)");
    email($register_data['email'], 'Activate your account', "
    Hello " . $register_data['first_name'] . ",\n\n
    You need to activate your account, so use the link below:\n\n
    http://http://starttutorials.local/phpacademy/register_login/activate.php?email=" . $register_data['email'] . "&email_code" . $register_data['email_code'] . "\n\n
    
    link
    
    - sparklet
    ");
}

function user_count() {
    $query = mysql_query("SELECT COUNT(`user_id`) 
                          FROM `users` 
                          WHERE `active` = 1");
    return mysql_result($query, 0);
}

function user_data($user_id) {
    $data = array();
    $user_id = (int) $user_id;

    $func_num_args = func_num_args();
    $func_get_args = func_get_args();
//    print_r($func_get_args);
    if ($func_num_args > 1) {
        unset($func_get_args[0]);

        $fields = '`' . implode('`, `', $func_get_args) . '`';
//        echo "SELECT $fields
//                              FROM `users` 
//                              WHERE `user_id`=$user_id";
//        die();
        $query = mysql_query("SELECT $fields
                              FROM `users` 
                              WHERE `user_id`=$user_id");
        $data = mysql_fetch_assoc($query);
//        print_r($data);
//        die();
        return $data;
    }
//    print_r($func_get_args);
}

function logged_in() {
    return (isset($_SESSION['user_id'])) ? true : false;
}

function user_exists($username) {
    $username = sanitize($username);
    $query = mysql_query("SELECT COUNT(`user_id`) 
        FROM `users` 
        WHERE `username`='$username'");
    return (mysql_result($query, 0) == 1) ? true : false;
}

function email_exists($email) {
    $email = sanitize($email);
    $query = mysql_query("SELECT COUNT(`user_id`) 
        FROM `users` 
        WHERE `email`='$email'");
    return (mysql_result($query, 0) == 1) ? true : false;
}

function user_active($username) {
    $username = sanitize($username);
    $query = mysql_query("SELECT COUNT(`user_id`) 
        FROM `users` 
        WHERE `username`='$username' AND `active`= 1");
    return (mysql_result($query, 0) == 1) ? true : false;
}

function user_id_from_username($username) {
    $username = sanitize($username);
    $query = mysql_query("SELECT `user_id` 
        FROM `users` 
        WHERE `username`='$username'");
    return mysql_result($query, 0, 'user_id');
}

function user_id_from_email($email) {
    $email = sanitize($email);
    $query = mysql_query("SELECT `user_id` 
        FROM `users` 
        WHERE `email`='$email'");
    return mysql_result($query, 0, 'user_id');
}

function login($username, $password) {
    $user_id = user_id_from_username($username);

    $username = sanitize($username);
    $password = md5($password);
    $query = mysql_query("SELECT COUNT(`user_id`) 
        FROM `users` 
        WHERE `username`='$username' AND `password`='$password'");

    return (mysql_result($query, 0) == 1) ? $user_id : false;
}

?>
